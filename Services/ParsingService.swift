//
//  RSSParser.swift
//  RSSReader
//
//  Created by Ivan Obodovskyi on 2/5/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import Foundation

struct RSSKeys {
    var title: String
    var publicationDate: String
    var description: String
}


final class ParsingService: NSObject, XMLParserDelegate {
    
    var xmlItems: [RSSKeys] = []
    
    private var currentElement = ""
    private var  currentTitle = "" {
        didSet {
            currentTitle = currentTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    private var currentDescription = "" {
        didSet {
            currentTitle = currentTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    private var currentPublicationDate = "" {
        didSet {
        currentTitle = currentTitle.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    private var parserCompletionHandler: (([RSSKeys]) -> Void)?
    
    func parseFeed(url: String, completionHandler: (([RSSKeys]) -> Void)?) {
        self.parserCompletionHandler = completionHandler
        
        let request = URLRequest(url: URL(string: url)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if let error = error {
                    print(error.localizedDescription)
                }
                return
            }
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        task.resume()
    }
    // MARK: - XMLParserDelegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentElement = elementName
        if currentElement == "entry" {
            currentTitle = ""
            currentDescription = ""
            currentPublicationDate = ""
        }
    }
    // Gets called when data inside tanks is getting parsed
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch currentElement {
        case "title":
            currentTitle += string
        case "updated":
            currentPublicationDate += string
        case "content":
            currentDescription += string
        default:
            break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "entry" {
            let rssItem = RSSKeys(title: currentTitle, publicationDate: currentPublicationDate, description: currentDescription)
            
            self.xmlItems.append(rssItem)
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        parserCompletionHandler?(xmlItems)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError.localizedDescription)
    }
}
