//
//  CellInformationUITaTableViewCell.swift
//  RSSReader
//
//  Created by Ivan Obodovskyi on 2/10/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class InformationTableViewCell: UITableViewCell {

    @IBOutlet weak var feedPublicationDateLabel: UILabel!
    @IBOutlet weak var feetTitleLabel: UILabel!
    
    var item: RSSKeys? {
        didSet {
            guard let rssItem = item else {
                print("Error")
                return
            }
            feedPublicationDateLabel.text = rssItem.publicationDate
            feetTitleLabel.text = rssItem.title
        }
    }
}
