//
//  FeedTableViewController.swift
//  RSSReader
//
//  Created by Ivan Obodovskyi on 2/10/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class FeedTableViewController: UITableViewController {
    
    private var rssItems: [RSSKeys?] = []
    private var urlToParse = "https://www.apple.com/newsroom/rss-feed.rss"

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
    }
    
    private func fetchData() {
        let feedParser = ParsingService()
        feedParser.parseFeed(url: urlToParse) { (rssItems) in
            self.rssItems = rssItems
            
            OperationQueue.main.addOperation {
                self.tableView.reloadSections(IndexSet(integer: 0), with: .left)
            }
        }
    }

    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShortInfoCell", for: indexPath) as? InformationTableViewCell
        
        if let item = rssItems[indexPath.item] {
            cell?.item = item
        }
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return rssItems.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailsVC") as? FeedDetailsViewController {
            vc.item = rssItems[indexPath.row]
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}


