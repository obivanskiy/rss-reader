//
//  FeedDetailsViewController.swift
//  RSSReader
//
//  Created by Ivan Obodovskyi on 2/11/19.
//  Copyright © 2019 Ivan Obodovskyi. All rights reserved.
//

import UIKit

class FeedDetailsViewController: UIViewController {
    
    @IBOutlet weak var feedTitleDetail: UILabel!
    @IBOutlet weak var feedDescriptionDetail: UILabel!
    @IBOutlet weak var feedPublicationDateDetail: UILabel!
    
    var item: RSSKeys! 
    

    override func viewDidAppear(_ animated: Bool) {
        feedTitleDetail.text = item.title
        feedPublicationDateDetail.text = item.publicationDate
        feedDescriptionDetail.text = item.description

    }
}
